#include "Header.h"
#include <iostream>
namespace gg
{
	void read(int& n, int mas[N][N])
	{
		std::cin >> n;
		for (int i = 0; i < n; ++i)
		{
			for (int j = 0; j < n; ++j)
			{
				std::cin >> mas[j][i];
			}
		}
	}

	void write(int n, int mas[N][N])
	{
		for (int i = 0; i < n; ++i)
		{
			for (int j = 0; j < n; ++j)
			{
				std::cout << mas[j][i] << ' ';
			}
			std::cout << std::endl;
		}
	}
	int count = 0;
	int check(int& n, int mas[N][N])
	{
		int s[N];
		for (int i = 0; i < n; ++i)
		{
			for (int j = 0; j < n - 1; ++j)
			{
				s[i] = std::max(mas[i][i], mas[i][j]);
			}
		}
		for (int i = 0; i < n; ++i)
		{
			if (s[i] == mas[i][i])
				count += 1;
		}
		return count;
	}

	int operation(int& n, int mas[N][N])
	{
		int q = 1;
		for (int i = 0; i < n; ++i)
		{
			q = q * std::max(mas[i][i], mas[i][i + 1]);
		}
		return q;
	}

	int swaper(int& n, int mas[N][N], int s)
	{
		for (int i = 0; i < n; ++i)
		{
			for (int j = 0; j < n; ++j)
			{
				int b = 0;
				b = mas[i][j];
				while (b > 0)
				{
					int h = 1;
					h = b % 10;
					if (h == 0)
					{
						mas[i][j] = s;
						break;
					}
					b /= 10;
				}
			}
		}
		return 0;
	}
}
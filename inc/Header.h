#ifndef _st_hpp
#define _st_hpp
#define N 100
#include <stdio.h>
#pragma once

namespace gg
{
	void read(int& n, int mas[N][N]);

	void write(int n, int mas[N][N]);

	int check(int& n, int mas[N][N]);

	int swaper(int& n, int mas[N][N], int s);

	int operation(int& n, int mas[N][N]);
}
#endif /* _st_hpp */
